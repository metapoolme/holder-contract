// SPDX-License-Identifier: MIT

pragma solidity 0.8.11;

import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";

contract Holder is Ownable, ReentrancyGuard {
    using SafeERC20 for IERC20;

    uint256 public constant DURATION = 365 * 1 days;
    IERC20 public mtp;

    struct LockedBunch {
        address user;
        uint256 amount;
        uint256 lockTime;
    }

    mapping(address => LockedBunch[]) public holders;
    mapping(address => uint256) private _userBalance;

    event LockedFunds(address indexed user, uint256 amount, uint256 timestamp);

    event Withdraw(address indexed to, uint256 amount);

    constructor(IERC20 tokenAddress) {
        mtp = tokenAddress;
    }

    function balanceOf(address account) external view returns (uint256 balance) {
        return _userBalance[account];
    }

    function allStakes(address _holder) external view returns (LockedBunch[] memory stakes) {
        return holders[_holder];
    }

    function lockedAmount(address _holder) public view returns (uint256 _lockedAmount) {
        LockedBunch[] memory userStakes = holders[_holder];

        for (uint256 i = 0; i < userStakes.length; i += 1) {
            uint256 lockTime = userStakes[i].lockTime;

            if (block.timestamp < (lockTime + DURATION) && lockTime != 0) {
                _lockedAmount += userStakes[i].amount;
            }
        }
    }

    function unlockedAmount(address _holder) public view returns (uint256 _unlockedAmount) {
        LockedBunch[] memory userStakes = holders[_holder];

        for (uint256 i = 0; i < userStakes.length; i += 1) {
            uint256 lockTime = userStakes[i].lockTime;

            if (block.timestamp >= (lockTime + DURATION) && lockTime != 0) {
                _unlockedAmount += userStakes[i].amount;
            }
        }
    }

    function lockFunds(uint256 _amount) external nonReentrant {
        require(_amount <= mtp.balanceOf(_msgSender()), "Holder: Cannot stake more than you own");

        _lockFunds(_amount);
    }

    function reLockFunds() external nonReentrant {
        require(unlockedAmount(_msgSender()) > 0, "Holder: no unlocked funds");

        LockedBunch[] storage userStakes = holders[_msgSender()];

        for (uint256 i = 0; i < userStakes.length; i += 1) {
            uint256 _lockTime = userStakes[i].lockTime;

            if (block.timestamp >= (_lockTime + DURATION) && _lockTime != 0) {
                userStakes[i].lockTime = block.timestamp;
            }
        }
    }

    function lockFundsFor(address _user, uint256 _amount) external {
        require(_msgSender() == address(mtp), "Holder: Only METAPOOL");
        require(_amount > 0, "Holder: Cannot stake nothing");

        uint256 timestamp = block.timestamp;

        holders[_user].push(LockedBunch(_user, _amount, timestamp));

        _userBalance[_user] += _amount;
        mtp.safeTransfer(owner(), _amount);

        emit LockedFunds(_user, _amount, timestamp);
    }

    function withdraw(uint256 amount) external nonReentrant {
        require(mtp.balanceOf(address(this)) >= amount, "Holder: insufficient amount in contract");
        uint256 withdrawalAmount = _withdraw(amount);

        mtp.safeTransfer(_msgSender(), withdrawalAmount);

        emit Withdraw(_msgSender(), amount);
    }

    function _withdraw(uint256 amount) private returns (uint256) {
        uint256 _amountFreed = unlockedAmount(_msgSender());
        require(_amountFreed >= amount, "Holder: not enough unlocked tokens");

        LockedBunch[] storage userStakes = holders[_msgSender()];

        uint256 emptyStakes = 0;

        for (uint256 i = 0; i < userStakes.length; i += 1) {
            // if the amount in the current element is more or equal the required amount,
            // we subtract the required amount and stop the cycle
            if (amount <= userStakes[i].amount) {
                userStakes[i].amount -= amount;
                _userBalance[_msgSender()] -= amount;
                break;
            }

            // If there are not enough tokens in the current element, we reset the current element and subtract
            // the number of tokens from the required number, the cycle continues
            if (amount > userStakes[i].amount) {
                amount -= userStakes[i].amount;
                _userBalance[_msgSender()] -= userStakes[i].amount;
                userStakes[i].amount = 0;
                emptyStakes += 1;
            }
        }

        // If stake is empty, 0, then remove it from the array of stakes
        if (emptyStakes > 0) {
            _removeEmptyStakes(userStakes);
        }

        return amount;
    }

    function _lockFunds(uint256 _amount) private {
        require(_amount > 0, "Holder: Cannot stake nothing");

        uint256 timestamp = block.timestamp;

        holders[_msgSender()].push(LockedBunch(_msgSender(), _amount, timestamp));

        _userBalance[_msgSender()] += _amount;

        mtp.safeTransferFrom(_msgSender(), owner(), _amount);

        emit LockedFunds(_msgSender(), _amount, timestamp);
    }

    function _removeEmptyStakes(LockedBunch[] memory stakes) private {
        LockedBunch[] storage userStakes = holders[_msgSender()];

        for (uint256 i = stakes.length; i > 0; i -= 1) {
            if (stakes[i - 1].amount == 0) {
                userStakes[i - 1] = userStakes[userStakes.length - 1];
                userStakes.pop();
            }
        }
    }
}